#!/usr/bin/python
# Author: Sandro Tosi <morph@debian.org>
# Date: 2009-09-30
# License: public domain
# 
# This script merges 2 lists of orphaned packages:
#
#  - the ones from http://qa.debian.org/data/orphaned.txt
#    (or from cli, if the file is already on the disk)
#  - the lists of package maintained by Debian QA Group
#
# then it tries to partition this set into many subsets
# in order to facilate the possible adoption by teams or
# people interested in particular packages areas

# for webpage output generation
from __future__ import with_statement
# for webpage date generation info
from datetime import datetime
# to know the machine
import platform
# module to access PostgreSQL databases
import psycopg2
# regexp
import re


def append_to_dict(dict, key, value, separator='<br/>'):
    """Appends a value to a dict entry (if it exists) or create the entry (if not); it creates the dictionary too, if it doesn't exist"""
    if dict is None:
        dict = {}
    if key in dict:
        dict[key] = dict[key] + separator + value
    else:
        dict[key] = value


def generate_webpage(maybe, orphans, uncat):
    webpage = '''<html>
<head><title>Orphaned Packages Grouping Up</title></head>
<body>

<center><h1>Orphaned Packages Grouping Up</h1></center>

<p>Report generated on %s UTC</p>

<p>This page tries to group up orphaned packages in sections, in order to easy their adoption. Below you can find a list of groups and a meta-group for package belonging to uncategorized sections.</p>

<p>The information are extracted from <a href=http://udd.debian.org>UDD</a> so they can a little bit outdated (given it's scheduled update every 6 hours).

<p><b>DISCLAIMER:</b> these groups are just approximations, so there might be some errors or incoherencies, so a direct check on the package is the ultimate solution to see if you'd like to adopt it.</p>

<p>For any questions, suggestions and improvements, please <a href="mailto:morph@debian.org">write me</a>. Git repository for the script generating this page is <a href=http://git.debian.org/?p=users/morph/qa_packages_analyze.git>here</a>.

<hr />

<h2>Total Orphaned Packages: %s</h2>

<hr />

<h2>Groups list</h2>''' % (datetime.utcnow(), len(orphans.keys()))

    groups = sorted(maybe.keys())

    webpage += '<ul>'
    for group in groups:
        webpage += '<li><a href="#%s">Group %s</a></li>' % (group, group)
    webpage += '</ul>'

    webpage += '''<a href="#uncategorized">Uncategorized</a>

<br /><br />
<hr />

<h2>Groups details</h2>'''

    for group in groups:
        g_keys = sorted(maybe[group].keys())
        pkgs = ' '.join(g_keys)
        webpage += '''<p><a name="%s"></a>
    <h3>Group: %s (pkgs: %s)</h3>
    <h4><a href="http://qa.debian.org/developer.php?packages=%s">DDPO Page</a></h4>
    <table border=1>
    <tr><td><b>Package</b></td><td><b>WNPP bug</b></td><td><b>Orphaned Since</b></td><td><b>In this group because</b></td></tr>''' % (group, group, len(g_keys), pkgs)

        for pkg in sorted(maybe[group].keys()):
            webpage += '''<tr><td>%s</td><td><a href=http://bugs.debian.org/%s>%s</a></td><td>%s</td><td>%s</td></tr>''' % (pkg, orphans[pkg]['bug'], orphans[pkg]['type'], orphans[pkg]['orphaned_time'], maybe[group][pkg])

        webpage += '''</table>
        </p>'''
    

    webpage += '''<a name="uncategorized"></a>
<h2>Uncategorized (pkgs: %s)</h2>

<table border=1>
<tr><td><b>Package</b></td><td><b>WNPP bug</b></td><td><b>Orphaned Since</b></td></tr>''' % len(uncat)

    for pkg in sorted(uncat):
        webpage += '''<tr><td>%s</td><td><a href=http://bugs.debian.org/%s>%s</a></td><td>%s</td></tr>''' % (pkg, orphans[pkg]['bug'], orphans[pkg]['type'], orphans[pkg]['orphaned_time'])

    webpage += '''</table>'''

    webpage += '''</body></html>'''

    return webpage

# MAIN #

if platform.node() == 'quantz':
    # connect for quantz
    dsn = 'dbname=udd port=5441 host=localhost user=guest'
else:
    # connect from my devel machine on local UDD replica
    dsn = 'dbname=udd'

# initialize variables used for db interaction
conn = cur = None

try:
    conn = psycopg2.connect(dsn)
    cur = conn.cursor()
    cur.execute('select source, type, bug, orphaned_time from orphaned_packages')
    #qa_pkgs = cur.fetchall()
    #cur.close()
    #print len(qa_pkgs)

    orphans = {}

    for item in cur.fetchall():
        orphans[item[0]] = {'type': item[1], 'bug': item[2], 'orphaned_time': item[3]}

    uncat = orphans.keys()
    
    #uncat = zip(*qa_pkgs)[0]
    maybe = {'python':{},'perl':{},'games':{},'lib':{},'sci':{},'gnome':{},'kde':{},'java':{},'ruby':{},'tex':{}}

    # debugging
    #import pdb;# pdb.set_trace()

    #print "I: Analyzing orphaned packages"

    for source in orphans:
        
        #print source, type, bug

        cur.execute("select section, build_depends, build_depends_indep from sources where source = '%s' AND distribution = 'debian' AND release = 'sid' " % (source))
        source_info = cur.fetchall()
        #print source_info

        try:
            section, build_depends, build_depends_indep = source_info[0]
        except:
            print "Error retriving information for %s (bug: http://bugs.debian.org/%s) - ignoring and continuing..." % (source, orphans[source]['bug'])
            continue

        # remove 'contrib/' 'non-free/' from section
        section = section.replace('contrib/','').replace('non-free/','')

        # trying to identify category from source package section
        if section == 'python':
            append_to_dict(maybe['python'], source, 'section = '+ section)
        elif section == 'perl':
            append_to_dict(maybe['perl'], source, 'section = '+ section)
        elif section == 'games':
            append_to_dict(maybe['games'], source, 'section = '+ section)
        elif section in ('libs', 'oldlibs', 'libdevel'):
            append_to_dict(maybe['lib'], source, 'section = '+ section)
        elif section in ('science', 'electronics', 'math'):
            append_to_dict(maybe['sci'], source, 'section = '+ section)
        elif section == 'gnome':
            append_to_dict(maybe['gnome'], source, 'section = '+ section)
        elif section == 'kde':
            append_to_dict(maybe['kde'], source, 'section = '+ section)
        elif section == 'tex':
            append_to_dict(maybe['tex'], source, 'section = '+ section)
        elif section == 'java':
            append_to_dict(maybe['java'], source, 'section = '+ section)
        else:
            # in case of uncategorized section, we print section
            #append_to_dict(uncat, section, '.', separator='')
            pass


        if build_depends is None:
            if build_depends_indep is None:
                b_d_ALL = ''
            else:
                b_d_ALL = build_depends_indep
        else:
            if build_depends_indep is None:
                b_d_ALL = build_depends
            else:
                b_d_ALL = build_depends + ', ' + build_depends_indep

        #print b_d_ALL

        # rimuovere tutti gli spazi?

        b_d_ALL = b_d_ALL.replace(' ', '').replace('|', ',')
        b_d_list = b_d_ALL.split(',')

        #print b_d_list

        # split by ',' , !
        # remove (.+) e [.+]

        for b_d in b_d_list:
            b_d = re.sub('\(.*\)', '', b_d)
            b_d = re.sub('\[.*\]', '', b_d)

            if b_d in ['python','python-all','python-dbg','python-dev']:
                append_to_dict(maybe['python'], source, 'build-depends on '+b_d)
            if b_d in ['perl']:
                append_to_dict(maybe['perl'], source, 'build-depends on '+b_d)
            if b_d in ['ruby']:
                append_to_dict(maybe['ruby'], source, 'build-depends on '+b_d)
            if b_d in ['sun-java5-jdk','sun-java6-jdk','java-gcj-compat','default-jdk', 'default-jdk-builddep', 'openjdk-6-jdk']:
                append_to_dict(maybe['java'], source, 'build-depends on '+b_d)

        ## BINARY PACKAGES ##

        cur.execute("select package, section from packages where source = '%s' AND distribution = 'debian' AND release = 'sid' AND architecture = 'i386' " % (source))
        binary_info = cur.fetchall()

        for bin, bin_section in binary_info:
            # remove 'contrib/' 'non-free/' from section
            bin_section = bin_section.replace('contrib/','').replace('non-free/','')

            # trying to identify category from source package bin_section
            if bin_section == 'python':
                append_to_dict(maybe['python'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section == 'perl':
                append_to_dict(maybe['perl'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section == 'games':
                append_to_dict(maybe['games'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section in ('libs', 'oldlibs', 'libdevel'):
                append_to_dict(maybe['lib'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section in ('science', 'electronics', 'math'):
                append_to_dict(maybe['sci'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section == 'gnome':
                append_to_dict(maybe['gnome'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section == 'kde':
                append_to_dict(maybe['kde'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section == 'tex':
                append_to_dict(maybe['tex'], source, 'bin_section (' + bin + ') = '+ bin_section)
            elif bin_section == 'java':
                append_to_dict(maybe['java'], source, 'bin_section (' + bin + ') = '+ bin_section)
            else:
                # in case of uncategorized bin_section, we print bin_section
                #append_to_dict(uncat, bin_section, '.', separator='')
                pass


    cur.close()
    conn.close()  
except Exception, e:
    # exec those only if they're instantiated
    if cur:  cur.close()
    if conn: conn.close()
    import traceback
    traceback.print_exc()

#from pprint import pprint

#pprint(maybe)

uncat_set = set(uncat)

for k in maybe:
    uncat_set -= set(maybe[k].keys())

uncat = list(uncat_set)


#print "I: Generating HTML report"

#mytemplate = Template(filename='qa_packages_analyze_UDD.mako')
#webpage = mytemplate.render(maybe=maybe,uncat=uncat,wnpp_bugs=wnpp_bugs)

with open('qa_packages_analyze.html','w') as f:
    f.write(generate_webpage(maybe, orphans, uncat))

#print "Report generated"
