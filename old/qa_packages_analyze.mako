<%
    import datetime

    def now():
        return datetime.datetime.now()
%>

<%def name="print_dict_table(adict)">
    % for key in sorted(adict.keys()):
      	  % if key in wnpp_bugs.keys():
	     <tr><td>${key}</td><td>${adict[key]}</td><td><a href=http://bugs.debian.org/${wnpp_bugs[key]['debbugsID'][0]}>${wnpp_bugs[key]['debbugsTitle'][0]}</a></td></tr>
	  % else:
	     <tr><td>${key}</td><td>${adict[key]}</td><td>?? No WNPP bugs ??</tr>
          % endif
    %endfor
</%def>

<html>
<head><title>Orphaned Packages Grouping Up</title></head>
<body>

<center><h1>Orphaned Packages Grouping Up</h1></center>

<p>Report generated on ${now()}</p>

<p>This page tries to group up orphaned packages in section, in order to easy their adoption. Below you can find a list of groups and a meta-group for package belonging to uncategorized sections.</p>

<p><b>DISCLAIMER:</b> these groups are just approximations, so there might be some errors or incoherencies, so a direct check on the package is the ultimate solution to see if you'd like to adopt it.</p>

<h2>Groups list</h2>

<ul>
% for i in sorted(maybe.keys()):
    <li><a href="#${i}">Group ${i}</a></li>
% endfor
</ul>

<a href="#uncategorized">Uncategorized</a>

<h2>Groups details</h2>

% for group in sorted(maybe.keys()):
    <p><a name="${group}"></a>
    <h3>Group: ${group}</h3>
    <h4><a href="http://qa.debian.org/developer.php?packages=${' '.join(sorted(maybe[group].keys()))}">DDPO Page</a></h4>
    <table border=1>
    <tr><td><b>Package</b></td><td><b>Reason</b></td><td><b>WNPP bug info</b></td></tr>
    ${print_dict_table(maybe[group])}
    </table>
    </p>
% endfor

<a name="uncategorized"></a>
<h2>Uncategorized</h2>

<table border=1>
<tr><td><b>Section</b></td><td><b>Packages Count</b></td></tr>
% for key in sorted(uncat.keys()):
    <tr><td>${key}</td><td>${len(uncat[key])}</td></tr>
%endfor
</table>

</body>
</html>
